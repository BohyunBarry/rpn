package calculator;

public class AppException extends Exception {

	/**
	 * 
	 */
	
	public AppException(String op, int pos) {
		super(String.format("operator %1$s (position: %2$d): insucient parameters", op, pos));
	}
}
