package calculator;

import operator.*;

public class Calculator {
	
	private static final char WHITESPACE = ' ';
	private static final String UNDO = "undo";
	private static final String CLEAR = "clear";
	private static final String REG = "^[+-]?(?:\\\\d+\\\\.?\\\\d*|\\\\d*\\\\.\\\\d+)$";
	
	private CalculatorStack cs;
	private OperatorManager om;
	
	public Calculator() {
		cs = new CalculatorStack();
		om = OperatorManager.getInstance();
	}
	
	/**
	 * parse the input
	 * */
	public void processText(String text) throws AppException{
		StringBuilder sBuilder = new StringBuilder();
		boolean isWord = false;
		for(int i = 0; i < text.length(); i++) {
			char c = text.charAt(i);
			
			if(!isWord) {
				if(c != WHITESPACE) {
					isWord = true;
					sBuilder.append(c);
				}
			}else {
				if(c == WHITESPACE) {
					isWord = false;
					processWord(sBuilder.toString(), i);
					sBuilder = new StringBuilder();
				}else {
					sBuilder.append(c);
				}
			}
		}
		
		if(isWord) {
			processWord(sBuilder.toString(), text.length());
		}
	}
	
	/**
	 * process the parsed text. if it is  a number, push it into the stack, if not, perform the corresponding operation**/
	private void processWord(String word, int pos) throws AppException{
		if(isRealNumber(word)) {
			cs.push(new StackElement(Double.parseDouble(word)));
		}else if(om.contains(word)) {
			applyOperator(word, pos);
		}else {
			switch(word) {
				case CLEAR:
					cs.clear();
					break;
				case UNDO:
					undo();
					break;
				default:
					throw new AppException(word, pos);
			}
		}
	}
	
	/**
	 * According to the operator to do the corresponding calculation**/
	private void applyOperator(String operator, int pos)throws AppException{
		AbstractOperator op = om.get(operator);
		
		if(op == null) {
			throw new AppException(operator, pos);
		}
		
		boolean result;
		
		try {
			result = op.execute(cs);
		}catch(ArithmeticException ae) {
			throw new AppException(operator, pos);
		}
		
		if(!result) {
			throw new AppException(operator, pos);
		}
	}
	
	/**
	 * restore the last operation. if the last operation is pushing a number, remove it number from stack. if the last operation is an operator, restore
	 * the value before the operator
	 * **/
	private void undo() {
		if(!cs.isEmpty()) {
			StackElement s = cs.pop();
			
			if(s.getParams() != null) {
				for(StackElement e : s.getParams()) {
					cs.push(e);
				}
			}
		}
	}
	
	/**
	 * determine a text is a number or not
	 * **/
	private boolean isRealNumber(String text) {
		return text != null && text.matches(REG);
	}
	
	/**
	 * print all number in the stack
	 * **/
	public void print() {
		cs.print();
	}
	
	/**
	 * return an array that all numbers in the stack
	 * **/
	public double[] getStackNum() {
		return cs.toArray();
	}
}
