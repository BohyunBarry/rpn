package calculator;
import java.util.Stack;

public class CalculatorStack {
	
	private static final String LABEL = "Stack:";
	private static final String SPACE = " ";
	private static final String REGEX_ZERO = "\\\\.*0*$";
	private static final String NONE = "";
	
	private Stack<StackElement> stack;
	
	public CalculatorStack() {
		stack = new Stack<>();
	}
	
	/**
	 * push an item onto the top of the stack
	 * **/
	public void push(StackElement element) {
		stack.push(element);
	}
	
	/*
	 * remove the top object of the stack and return that object as the value of this function
	 * */
	public StackElement pop() {
		return stack.pop();
	}
	
	/*
	 * determine the stack is empty or not
	 * */
	public boolean isEmpty() {
		return stack.isEmpty();
	}
	
	/*
	 * count the elements in the stack
	 * */
	public int size() {
		return stack.size();
	}
	
	/*
	 * clear the stack
	 * */
	public void clear() {
		stack.clear();
	}
	
	/*
	 * convert all the numbers to an array in the stack
	 * */
	public double[]toArray(){
		double[] result = new double[stack.size()];
		int i = 0;
		
		for(StackElement element : stack) {
			result[i] = element.getValue();
			i++;
		}
		
		return result;
	}
	
	/*
	 * print all values in the stack
	 * */
	public void print() {
		System.out.print(LABEL);
		
		for(StackElement element : stack) {
			System.out.print(SPACE);
			System.out.print(formatValue(element.getValue()));
		}
		
		System.out.println();
	}
	
	/*
	 * format a number up to 10 precision
	 * */
	private String formatValue(double value) {
		String text = String.format("%.10f", value);
		
		return text.replaceAll(REGEX_ZERO, NONE);
	}
}
