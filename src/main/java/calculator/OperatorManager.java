package calculator;
import java.util.HashMap;
import java.util.Map;

import operator.AbstractOperator;
import operator.AddOperator;
import operator.DivisionOperator;
import operator.MultipOperator;
import operator.SqrtOperator;
import operator.SubtractionOperator;

public class OperatorManager {
	
	private static final OperatorManager instance = new OperatorManager();
	
	private static final String ADD = "+";
	private static final String SUB = "-";
	private static final String MULTI = "*";
	private static final String DIV = "/";
	private static final String SQRT = "sqrt";
	
	private Map<String, AbstractOperator> oMap;
	
	private OperatorManager() {
		oMap = new HashMap<>();
		oMap.put(ADD, new AddOperator());
		oMap.put(SUB, new SubtractionOperator());
		oMap.put(MULTI, new MultipOperator());
		oMap.put(DIV, new DivisionOperator());
		oMap.put(SQRT, new SqrtOperator());
	}
	
	public static final OperatorManager getInstance() {
		return instance;
	}
	
	/*
	 * check the map contains the operator or not
	 * */
	public boolean contains(String key) {
		return oMap.containsKey(key);
	}
	
	/*
	 * get an operator by the key
	 * */
	public AbstractOperator get(String key) {
		return oMap.get(key);
	}
}
