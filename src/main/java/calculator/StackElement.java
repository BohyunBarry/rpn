package calculator;

public class StackElement {
	private double value;
	private StackElement[] params;
	
	public StackElement() {
    }
	
	public StackElement(double value) {
		this();
		this.value = value;
	}
	
	public StackElement(double value, StackElement param) {
		this(value);
		params = new StackElement[1];
		params[0] = param;
	}
	
	public StackElement(double value, StackElement first, StackElement second) {
		this(value);
		params = new StackElement[2];
		params[0] = first;
		params[1] = second;
	}
	
	public StackElement(double value, StackElement[] params) {
		this(value);
		this.params = params;
	}
	
	public double getValue() {
		return value;
	}
	
	public void setValue(double value) {
		this.value = value;
	}
	
	public StackElement[] getParams() {
		return params;
	}
	
	public void setParams(StackElement[] params) {
		this.params = params;
	}
}
