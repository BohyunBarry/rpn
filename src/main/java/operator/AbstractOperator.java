package operator;

import calculator.CalculatorStack;
import calculator.StackElement;

public abstract class AbstractOperator {
	
	/*
	 * Calculation
	 * */
	public boolean execute(CalculatorStack stack) {
		if(countParams() > stack.size()) {
			return false;
		}
		
		stack.push(calculate(stack));
		
		return true;
	}
	
	/*
	 * implement it in the implementation class
	 * */
	protected abstract StackElement calculate(CalculatorStack stack);
	
	protected int countParams() {
		return 2;
	}
}
