package operator;

import calculator.CalculatorStack;
import calculator.StackElement;

public class AddOperator extends AbstractOperator {

	@Override
	protected StackElement calculate(CalculatorStack stack) {
		// TODO Auto-generated method stub
		StackElement second = stack.pop();
		StackElement first = stack.pop();
		
		double value = first.getValue() + second.getValue();
		
		return new StackElement(value, first, second);
	}

}
