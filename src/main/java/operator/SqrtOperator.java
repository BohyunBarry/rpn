package operator;

import calculator.CalculatorStack;
import calculator.StackElement;

public class SqrtOperator extends AbstractOperator {

	@Override
	protected StackElement calculate(CalculatorStack stack) {
		// TODO Auto-generated method stub
		StackElement param = stack.pop();
		
		if(param.getValue() < 0) {
			throw new ArithmeticException();
		}
		
		double value = Math.sqrt(param.getValue());
		
		return new StackElement(value, param);
	}

	@Override
	protected int countParams() {
		// TODO Auto-generated method stub
		return 1;
	}
	
	

}
