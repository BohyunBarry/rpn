package operator;

import org.junit.Test;

import calculator.CalculatorStack;
import calculator.StackElement;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertFalse;

public class MultipOperatorTest {
	  @Test
	    public void operationHasTwoParameters() {
		  MultipOperator operator = new MultipOperator();
	        CalculatorStack stack = new CalculatorStack();
	        stack.push(new StackElement(5));
	        stack.push(new StackElement(3));
	        if (operator.execute(stack)) {
	            assertEquals(1, stack.size());
	            StackElement e = stack.pop();
	            assertEquals(15.0, e.getValue());
	            StackElement[] arr = e.getParams();
	            assertEquals(2, arr.length);
	            assertEquals(5.0, arr[0].getValue());
	            assertEquals(3.0, arr[1].getValue());
	        } else {
	            fail("Multiplication can be done with two parameters.");
	        }

	    }

	    @Test
	    public void operationHasOneParameter() {
	    	MultipOperator operator = new MultipOperator();
	        CalculatorStack stack = new CalculatorStack();
	        stack.push(new StackElement(5));
	        assertFalse("Multiplication can not be done with one parameter.", operator.execute(stack));
	    }
}
