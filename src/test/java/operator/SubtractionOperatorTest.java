package operator;

import org.junit.Test;

import calculator.CalculatorStack;
import calculator.StackElement;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertFalse;

public class SubtractionOperatorTest {
	@Test
    public void operationHasTwoParameters() {
        SubtractionOperator operator = new SubtractionOperator();
        CalculatorStack stack = new CalculatorStack();
        stack.push(new StackElement(5));
        stack.push(new StackElement(2));
        if (operator.execute(stack)) {
            assertEquals(1, stack.size());
            StackElement e = stack.pop();
            assertEquals(3.0, e.getValue());
            StackElement[] arr = e.getParams();
            assertEquals(2, arr.length);
            assertEquals(5.0, arr[0].getValue());
            assertEquals(2.0, arr[1].getValue());
        } else {
            fail("Subtraction can be done with two parameters.");
        }

    }

    @Test
    public void operationHasOneParameter() {
        AddOperator operator = new AddOperator();
        CalculatorStack stack = new CalculatorStack();
        stack.push(new StackElement(5));
        assertFalse("Subtraction can not be done with one parameter.", operator.execute(stack));
    }
}
